package it.source.myapplication;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;




public class MainActivity extends AppCompatActivity {

    EditText name;
    EditText day;
    EditText month;
    EditText year;

    Button activity;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

         day = findViewById(R.id.day);
         month = findViewById(R.id.month);
         year = findViewById(R.id.year);
        View activity = findViewById(R.id.activity);


        activity.setOnClickListener(new View.OnClickListener() {
            //@override
            public void onClick(View v) {
                if (!name.getText().toString().isEmpty() && !day.getText().toString().isEmpty() && !month.getText().toString().isEmpty() && !year.getText().toString().isEmpty()) {
                    String Name = name.getText().toString();
                    int Day = Integer.parseInt(day.getText().toString());
                    int Month = Integer.parseInt(month.getText().toString());
                    int Year = Integer.parseInt(year.getText().toString());



                    Intent intent = new Intent(MainActivity.this,SecondActivity.class);
                    intent.putExtra("name", Name);
                    intent.putExtra("day", Day);
                    intent.putExtra("month", Month);
                    intent.putExtra("year", Year);
                    startActivity(intent);
                } else {
                    new AlertDialog.Builder(MainActivity.this)
                            .setTitle("Ошибка")
                            .setMessage("Заполните все поля!!")
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }).show();
                }
            }
        });
    }
}

//1. Создать приложение с двумя экранами.
// На первом экране пользователь вводит свое имя и дату рождения и нажимает кнопку “продолжить”.
// Затем открывается второй экран, на котором пользователь видит свою статистику по прожитому времени (сколько ему полных лет, сколько месяцев он прожил, сколько дней ему исполнилось в момент открытия статистики).

//2*. Расположить на экране два контейнера на каждом по три текстовых поля для ввода цифр, на первом контейнере есть еще и три кнопки. После ввода цифр в десятичном формате и нажатии соответствующей кнопки в первом контейнере - во втором контейнере соответствующее поле заполняется тем же числом, но в другой системе счисления (двоичной, восьмеричной и шестнадцатеричной соответственно).
//2.1** Реализовать тоже задание, но без кнопок. Цифры преобразуются автоматически, каждый раз, как только пользователь вводит новые данные (изменения отслеживаются с помощью TextWatcher).