package it.source.myapplication;


import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class SecondActivity extends AppCompatActivity {

    TextView information;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.second_main);

        information = findViewById(R.id.information);

        Bundle arguments = getIntent().getExtras();

        String name = arguments.get("name").toString();
        int day = arguments.getInt("day");
        int month = arguments.getInt("month");
        int year = arguments.getInt("year");

        Date currentDate = new Date();
        DateFormat ifDay = new SimpleDateFormat("dd", Locale.getDefault());
        String currentDay = ifDay.format(currentDate);
        int iDay = Integer.parseInt(currentDay);
        DateFormat dfMonth = new SimpleDateFormat("MM", Locale.getDefault());
        String currentMonth = dfMonth.format(currentDate);
        int  iMonth = Integer.parseInt(currentMonth);
        DateFormat dfYear = new SimpleDateFormat("yyyy", Locale.getDefault());
        String currentYear = dfYear.format(currentDate);
        int iYear = Integer.parseInt(currentYear);

        if (day > iDay) {
            month -= 1;
            day = 30 + iDay - day;
        } else {
            day = iDay - day;
        }

        if (month == 0) {
            year -= 1;
            month = 12;
        }
        if (month > iMonth) {
            year += 1;
            month = iMonth + 12 - month;
        } else {
            month = iMonth - month;
        }
        year = iYear - year;

        information.setText(name + ", вы прожили: " + day + " день (дня/дней) " + month + " месяц (месяца/месяцев) " + year + " год (года/годов)");
    }
}
